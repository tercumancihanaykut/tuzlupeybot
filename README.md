# Tuzlupeybot

[Tuzlu Peynir](https://discord.gg/0bF69kqCWd4z5sEc) Discord kanalının en yetkili botudur.

Tüm Tuzlu Peynir halkı bota istediği özelliği ekleyebilir.

### Sistem Gereksinimleri

- [Node.js](https://nodejs.org)

### Kurulum

```sh
$ git clone git@gitlab.com:denizcdemirci/tuzlupeybot.git
$ cd tuzlupeybot
$ npm install
```

### Geliştirme

Tuzlupeybot, [Discord.js](https://discord.js.org) kullanılarak yazılmıştır. Ayrıca kullanım örnekleri için [Discord.js Guide](https://discordjs.guide) bakabilirsiniz.

### Deploy

Gönderdiğiniz merge request onaydan sonra [Heroku](https://herokuapp.com)'ya otomatik deploy olacaktır.
